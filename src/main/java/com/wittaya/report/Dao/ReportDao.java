/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.report.Dao;

import com.wittaya.report.helper.DatabaseHelper;
import com.wittaya.report.model.ReportModel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AdMiN
 */
public class ReportDao {
    public List<ReportModel> getDayReport(){
        ArrayList<ReportModel> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", InvoiceDate) as period, SUM(Total) as total FROM invoices "
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC\n"
                + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()){
                ReportModel item = ReportModel.fromRS(rs);
                list.add(item);
            }
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportModel> getMonthReport(int year){
        ArrayList<ReportModel> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", InvoiceDate) as period, SUM(Total) as total FROM invoices "
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "WHERE strftime(\"%Y\", InvoiceDate)=\"" + year + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC\n"
                + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()){
                ReportModel item = ReportModel.fromRS(rs);
                list.add(item);
            }
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
