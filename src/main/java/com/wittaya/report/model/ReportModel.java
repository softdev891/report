/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.report.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AdMiN
 */
public class ReportModel {
      String period;
    double total;

    public ReportModel() {
         this.period = period;
        this.total = total;
    }

    public ReportModel(String period, double total) {
        this.period = period;
        this.total = total;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReportSale{" + "period=" + period + ", total=" + total + '}';
    }

    public static ReportModel fromRS(ResultSet rs) {
       ReportModel obj = new ReportModel();
       try{
           obj.setPeriod(rs.getString("period"));
           obj.setTotal(rs.getDouble("total"));
           return obj;
       } catch (SQLException ex){
          Logger.getLogger(ReportModel.class.getName()).log(Level.SEVERE, null, ex);
       }
       return obj;
    }

}
