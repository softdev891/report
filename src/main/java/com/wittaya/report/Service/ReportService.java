/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.report.Service;

import com.wittaya.report.Dao.ReportDao;
import com.wittaya.report.model.ReportModel;
import java.util.List;

/**
 *
 * @author AdMiN
 */
public class ReportService {
     public List<ReportModel> getReportSaleByDay() {
        ReportDao reportDao = new ReportDao();
        return reportDao.getDayReport();
    }

    public List<ReportModel> getReportSaleByMonth(int year) {
        ReportDao dao = new ReportDao();
        return dao.getMonthReport(year);
    }

}
